var apiBaseUrl = 'http://0.0.0.0:3000'

$(document).ready(function(){
    
	detectLoggedUser();
	
	setLoadEvents();

	loadSectionMatches();
	
	findInboxCount();
});

function findInboxCount(){
	
	loggedUser = getLoggedProfile();

	$.getJSON(apiBaseUrl + '/inbox?callback=?', { user_id: loggedUser.user_id  },
		function(data) {
			updateInboxCount(data.users.length);
		}
	);
}

function detectLoggedUser(){
	if(getLoggedProfile() != null){
		showSection("layout");
	} 
}

function loadSectionMatches(){
	console.log('### Section: MATCHES ###');
	sectionId = "#layout-matches";

	addLoading(sectionId);
	
	loadMoreProfiles(sectionId);
}

var profiles = [];

function loadMoreProfiles(sectionId){
	loggedUser = getLoggedProfile();
	$.getJSON(apiBaseUrl + '/profiles.json?callback=?', { user_id: loggedUser.user_id },
		function(data) {
			profiles = data.profiles;
			showNextProfile();
			removeLoading(sectionId)
		}
	);
}

function showNextProfile(){
	var match_template = _.template(document.getElementById("match_template").innerHTML);
	
	if(profiles.length > 0){
		$('#no-matches-results').addClass('hide');
		$('#matches-results').show();
			
			
		$('#matches-results').html('');
		
		// Take the first one
		current_profile = profiles[0];
			
		$('#matches-results').html(match_template(current_profile)).hide().fadeIn(1000);
		
		Lungo.dom('#matches-results #positive-match').on('tap', function(event){
			matchProfile(current_profile.user_id, true);
		});
		
		Lungo.dom('#matches-results #negative-match').on('tap', function(event){
			skipProfile(current_profile.user_id);
		});
		
		// Remove the element
		profiles.shift()
	} else {				
		$('#no-matches-results').removeClass('hide');
		$('#matches-results').hide();
	}
	
}

function performLogin(email,password){

	$('#login-button').hide();
	$('#register-button').hide();
	
	$.getJSON(apiBaseUrl + '/getprofile.json?callback=?', { email: email, password: password  },
		function(data) {
			// We're always gonna get one just in case
			window.sessionStorage.setItem('myProfile', JSON.stringify(data.profile));
			
			$('#login-button').show();
			$('#register-button').show();
			
			showSection("layout");
		}
	);
}

function getLoggedProfile(){
	return JSON.parse(window.sessionStorage.getItem('myProfile'));
}

function matchProfile(id, showOtherProfile){
	console.log('Matching profile: ' + id);
	
	var loggedUser = getLoggedProfile();

	$.getJSON(apiBaseUrl + '/matcher?callback=?', { from_user_id: loggedUser.user_id, to_user_id: id  },
		function(data) {
			if(showOtherProfile){
				showNextProfile();
			}

			if(data.match){
				Lungo.Notification.show(
				    "check",
				    "Matched!",
				    2
				);
			}
		}
	);

}

function skipProfile(id){
	console.log('Skiping profile: ' + id);
	
	showNextProfile();
}

function loadSectionLive(){
	console.log('### Section: LIVE ###');
	sectionId = "#layout-live";

	addLoading(sectionId);
	
	var loggedUser = getLoggedProfile();
	
	$('#live-online').hide();
	$('#live-offline').hide();
		
	console.log(loggedUser.status);
	
	if (loggedUser.status == "online") {
		$('#live-online').show();
		$('#live-offline').hide();
		
		$.getJSON(apiBaseUrl + '/allactive?callback=?', { user_id: loggedUser.user_id },
			function(data) {
				showEsriMap(data.positions);
			}
		);
	} else {
		// TC network is showing TX instead of NY, harcoding just for hackathon
		setLocation(40.752635, -73.994025)

		/*
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(successGeolocate, errorGeolocate, {
				  enableHighAccuracy: true,
				  timeout: 5000,
				  maximumAge: 0
				});
		} else {
			errorGeolocate('not supported');
		}
		*/
	}
		
	setTimeout(function(){ 
		removeLoading(sectionId)
	}, 700);
	
}

function successGeolocate(position){
	setLocation(position.coords.latitude, position.coords.longitude);	
}

function errorGeolocate(msg){
	setLocation(40.752635, -73.994025);	
}

function setLocation(lat,long){
	$('.location #location-lat').html(lat);
	$('.location #location-long').html(long);
	
	window.sessionStorage.setItem('user_lat', lat);
	window.sessionStorage.setItem('user_long', long);
	
	$('#live-online').hide();
	$('#live-offline').show();
}

function performGoOnline(rad){
	console.log('Go Online rad: ' + rad);
	
	loggedUser = getLoggedProfile();
	lat = window.sessionStorage.getItem('user_lat');
	long = window.sessionStorage.getItem('user_long');
		
	$.getJSON(apiBaseUrl + '/activate?callback=?', { user_id: loggedUser.id, lat: lat, long: long, rad: rad  },
		function(data) {
			$.getJSON(apiBaseUrl + '/allactive?callback=?', { user_id: loggedUser.user_id },
				function(data) {
					showEsriMap(data.positions);
				}
			);
			
			$('#live-online').show();
			$('#live-offline').hide();
		}
	);
}

function hideEsriMap(){
	$('#map').hide();
}

function showEsriMap(profiles){
	$('#map').show();
		
	var map;
    
	require([
	         "esri/map", 
	         "esri/geometry/Point", 
	         "esri/symbols/PictureMarkerSymbol", 
	         "esri/graphic",
	         "dojo/_base/array", 
	         "dojo/dom-style", 
	         "dojo/domReady!"
    ], function(
    		Map, 
    		Point,
    		PictureMarkerSymbol, 
    		Graphic,
    		arrayUtils, 
    		domStyle, 
    		ColorPicker
    ) {
		map = new Map("map",{
			basemap: "streets",
			// Centered in NY just for hackathon
			center: [-73.994025, 40.752635],
			zoom: 13,
			minZoom: 2
		});
      
		map.on("load", function(){
			var initColor = "#ce641d";
			var ms = new PictureMarkerSymbol('static/images/map-marker.png',25,40)
			arrayUtils.forEach(profiles, function(profile) {
				  
				var infoTemplate = new esri.InfoTemplate();
				infoTemplate.setTitle(profile.name);
				
				var tagsString = profile.tags.join(',');
				
				infoTemplate.setContent("<img src='static/faces/" + profile.image_url +"' class='map-image' /><hr>" + "<strong>" + profile.company_name + "</strong> | " + profile.company_position +"<br/>"
						+ "<strong>Tags:</strong> " + tagsString + "<br>"
						+ "<a onclick='matchProfile(" + profile.user_id + ", false); return false;' id='map-match-" + profile.user_id + "' class='map-match' rel='" + profile.user_id + "' href='#'>Match!</a><br>"
						+ "<a onclick='skipProfile(" + profile.user_id + "); return false;'id='map-skip-" + profile.user_id + "'class='map-skip' rel='"+ profile.user_id + "' href='#'>Skip</a>");

				var graphic = new Graphic(new Point([profile.lat, profile.long]), ms,null,infoTemplate);
				map.graphics.add(graphic);
			});
		});
	});
}

function performGoOffline(){
	console.log('Go Offline');
	
	loggedUser = getLoggedProfile();
	
	$.getJSON(apiBaseUrl + '/deactivate?callback=?', { user_id: loggedUser.id },
		function(data) {
			$('#live-offline').show();
			$('#live-online').hide();
			
			hideEsriMap();
		}
	);
}

function performLogout(){
	window.sessionStorage.clear();
	showSection("splash");
}

function loadSectionInbox(){
	console.log('### Section: SECTIONS ###');
	sectionId = "#layout-inbox";

	addLoading(sectionId);
	
	loggedUser = getLoggedProfile();
	
	$.getJSON(apiBaseUrl + '/inbox?callback=?', { user_id: loggedUser.user_id  },
		function(data) {
			renderInbox(data.users);
			updateInboxCount(data.users.length);
		}
	);
	
	setTimeout(function(){ 
		removeLoading(sectionId)
	}, 500);
	
}

function updateInboxCount(count){
	console.log('Updating count to: ' + count);
	$('.count').html(count);
}

function renderInbox(profiles){
	var inbox_template = _.template(document.getElementById("inbox_template").innerHTML);
	$(profiles).each(function(i,profile){
		$('#inbox-list').append(inbox_template(profile));
		
		Lungo.dom('#arrange-meeting-' + profile.user_id).on('tap', function(event){
			Lungo.Notification.show(
			    "envelope",
			    "Coming Soon :)",
			    3
			);
		});
	});
}

function addLoading(section){
	$(section + ' .loading').show();
	$(section + ' .empty').hide();
}

function removeLoading(section){
	$(section + ' .loading').hide();
	$(section + ' .empty').show();
}