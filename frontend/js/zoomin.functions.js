function showSection(id){
	Lungo.Router.section(id);
}

function setLoadEvents(){
	Lungo.dom('#splash').on('unload', function(event){
		loadSectionMatches();
	});
	
	Lungo.dom('#layout-matches').on('load', function(event){
		loadSectionMatches();
	});
	
	Lungo.dom('#layout-live').on('load', function(event){
		loadSectionLive();
	});
	
	Lungo.dom('#layout-inbox').on('load', function(event){
		loadSectionInbox();
	});
	
	Lungo.dom('#layout-settings').on('load', function(event){
		loadSectionSettings();
	});
	
	Lungo.dom('#refresh-profiles').on('tap', function(){
		addLoading('#layout-matches');
		loadMoreProfiles('#layout-matches');
	});

	Lungo.dom('#login-button').on('tap', function(event){
		performLogin($('#txt-signup-name').val(),$('#txt-signup-password').val());
		event.preventDefault();
	});
	
	Lungo.dom('#go-online-button').on('tap', function(event){
		performGoOnline($('#txt-radius').val());
		event.preventDefault();
	});
	
	Lungo.dom('#go-offline-button').on('tap', function(event){
		performGoOffline();
		event.preventDefault();
	});
	
	Lungo.dom('#logout-button').on('tap', function(event){
		performLogout();
		event.preventDefault();
	});
}