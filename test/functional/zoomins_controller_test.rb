require 'test_helper'

class ZoominsControllerTest < ActionController::TestCase
  setup do
    @zoomin = zoomins(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:zoomins)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create zoomin" do
    assert_difference('Zoomin.count') do
      post :create, zoomin: { first_meeter_id: @zoomin.first_meeter_id, second_meeter_id: @zoomin.second_meeter_id, status: @zoomin.status }
    end

    assert_redirected_to zoomin_path(assigns(:zoomin))
  end

  test "should show zoomin" do
    get :show, id: @zoomin
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @zoomin
    assert_response :success
  end

  test "should update zoomin" do
    put :update, id: @zoomin, zoomin: { first_meeter_id: @zoomin.first_meeter_id, second_meeter_id: @zoomin.second_meeter_id, status: @zoomin.status }
    assert_redirected_to zoomin_path(assigns(:zoomin))
  end

  test "should destroy zoomin" do
    assert_difference('Zoomin.count', -1) do
      delete :destroy, id: @zoomin
    end

    assert_redirected_to zoomins_path
  end
end
