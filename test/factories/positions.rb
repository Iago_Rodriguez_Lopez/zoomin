# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :position do
    lat 1
    long 1
    radio 1
    user_id 1
  end
end
