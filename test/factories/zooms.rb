# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :zoom do
    first_meeter_id 1
    second_meeter_id 1
    status "MyString"
  end
end
