# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :profile do
    company_name "MyString"
    company_position "MyString"
    description "MyText"
    user_id 1
    status "MyString"
  end
end
