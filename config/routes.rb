Zoomin::Application.routes.draw do
  get :matcher, :to => "zooms#matcher"
  get :activate, :to => "positions#setactive"
  get :deactivate, :to => "positions#setunactive"
  get :inbox,:to => "users#inbox"
  get :allactive, :to => "positions#allactive"

  resources :zoomins


  resources :positions


  resources :profiles
  get :getprofile, :to => "profiles#getprofile"
  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"
  devise_for :users
  resources :users
end