class Zoomin < ActiveRecord::Base
  attr_accessible :first_meeter_id, :second_meeter_id, :status
  belongs_to :first_meeter, :class_name => 'User', :foreign_key => 'first_meeter_id'
  belongs_to :second_meeter, :class_name => 'User', :foreign_key => 'second_meeter_id'

end
