class Profile < ActiveRecord::Base
  attr_accessible :company_name, :company_position, :description, :status, :user_id
  belongs_to :user
end
