class Position < ActiveRecord::Base
  attr_accessible :lat, :long, :radio, :user_id
  belongs_to :user
end
