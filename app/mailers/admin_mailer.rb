class AdminMailer < ActionMailer::Base
  include ActionView::Helpers::TextHelper
  default :from => Rails.env.development? ? 'info@zoomin.com' : 'info@zoomin.com'
  default :to => Rails.env.development? ? 'info@zoomin.com' : 'info@zoomin.com'
  layout "mail_layout"

  def new_match(user_id,user2_id)
    @user1 = User.find(user_id)
    @user2 = User.find(user2_id)

    mail(subject: "Zoomin", bcc: "#{@user1.email},#{@user2.email}")
  end

end