class UserSerializer < ActiveModel::Serializer
  attributes :user_id, :company_name,:company_position, :description, :image_url, :name
  def company_name
    object.profile.company_name 
  end

  def user_id
  	object.id
  end

  def company_position
    object.profile.company_position 
  end

  def description
    object.profile.description 
  end

  def image_url
    object.profile.image_url 
  end

  def tags
  	object.user.interests.pluck(:name) if object.user && object.user.interests
  end
end
