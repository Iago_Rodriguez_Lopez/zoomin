class ProfileSerializer < ActiveModel::Serializer
  attributes :id, :company_name,:company_position, :description, :image_url ,:status, :status, :name, :tags, :user_id
 def name
    object.user.name if object.user
  end

  def tags
  	object.user.interests.pluck(:name) if object.user
  end

  def status
  	if object.user and object.user.position
  		object.user.position.status 
  	else
  		"offline"
  	end
  end
end
