class PositionSerializer < ActiveModel::Serializer
  attributes :id, :company_name,:company_position, :description, :image_url, :tags, :user_id, :lat, :long, :radio, :name
  def company_name
    object.user.profile.company_name if object.user
  end

  def company_position
    object.user.profile.company_position if object.user
  end

  def description
    object.user.profile.description if object.user
  end

  def image_url
    object.user.profile.image_url if object.user
  end

  def tags
  	object.user.interests.pluck(:name) if object.user && object.user.interests
  end

  def name
    object.user.name
  end

end
