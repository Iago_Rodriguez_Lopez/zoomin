class ZoomsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers

  # For all responses in this controller, return the CORS access control headers.
  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.

  def cors_preflight_check
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
    headers['Access-Control-Max-Age'] = '1728000'
  end


  def index
    @zooms = Zoom.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @zooms }
    end
  end


  # POST /zoomins
  # POST /zoomins.json
  def matcher
    #from_user_id to_user_id
    @zoom = Zoom.where("(first_meeter_id == ? or second_meeter_id == ?) and status == ?", params[:user_id], params[:user_id], "matched")
    if !@zoom.empty?
      respond_to do |format|
        if @zoom.update_attributes(:status => "matched")
          AdminMailer.new_match(params[:to_user_id],params[:from_user_id]) 

          format.json { render json: {match: true}, :callback => params['callback'] }
        else
          format.json { render json: {match: false}, :callback => params['callback'] }
        end
      end
      #cambia estado a matched
    else
      #añades un posible match
      @zoom = Zoom.new(:first_meeter_id => params[:from_user_id], :second_meeter_id => params[:to_user_id], :status => "waiting")
      respond_to do |format|
        if @zoom.save
          format.json { render json: {match: false}, :callback => params['callback'] }
        else
          format.json { render json: {match: false}, :callback => params['callback'] }
        end
      end
    end
  end

end
