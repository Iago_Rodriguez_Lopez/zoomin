class PositionsController < ApplicationController
  # GET /positions
  # GET /positions.json
  skip_before_filter :verify_authenticity_token
  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers

  # For all responses in this controller, return the CORS access control headers.
  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.

  def cors_preflight_check
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version'
    headers['Access-Control-Max-Age'] = '1728000'
  end



  # POST /positions
  # POST /positions.json
  def setactive
    @user = User.find(params[:user_id])
    @user.position.lat =params[:lat]
    @user.position.long =params[:long]
    @user.position.radio =params[:rad]
    @user.position.status = "online"

    respond_to do |format|
      if @user.save
        format.json { render json: @user.position, :callback => params['callback'] }
      else
        format.json { render json: @user.position.errors, status: :unprocessable_entity , :callback => params['callback']}
      end
    end
  end

  def allactive
    respond_to do |format|
        format.json { render json: Position.where(:status => "online"), :callback => params['callback'] }
    end
  end

  def setunactive
    @user = User.find(params[:user_id])
    @user.position.status = "offline"

    respond_to do |format|
      if @user.save
        format.json { render json: @user.position, status: :created, location: @position, :callback => params['callback'] }
      else
        format.json { render json: @user.position.errors, status: :unprocessable_entity, :callback => params['callback'] }
      end
    end
  end


  def index
    @positions = Position.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @positions }
    end
  end

  # GET /positions/1
  # GET /positions/1.json
  def show
    @position = Position.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @position }
    end
  end

  # GET /positions/new
  # GET /positions/new.json
  def new
    @position = Position.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @position }
    end
  end

  # GET /positions/1/edit
  def edit
    @position = Position.find(params[:id])
  end


  # PUT /positions/1
  # PUT /positions/1.json
  def update
    @position = Position.find(params[:id])

    respond_to do |format|
      if @position.update_attributes(params[:position])
        format.html { redirect_to @position, notice: 'Position was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @position.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /positions/1
  # DELETE /positions/1.json
  def destroy
    @position = Position.find(params[:id])
    @position.destroy

    respond_to do |format|
      format.html { redirect_to positions_url }
      format.json { head :no_content }
    end
  end
end
