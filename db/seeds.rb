# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Environment variables (ENV['...']) can be set in the file config/application.yml.
# See http://railsapps.github.io/rails-environment-variables.html
user = User.new
user.name = Faker::Name::first_name 
user.email = user.name+"@factory.com"
user.password = "password"
profile = user.build_profile
profile.company_name = Faker::Company.name
profile.image_url = rand(1..19).to_s+".jpg"
profile.company_position = Faker::Name.title
profile.description = Faker::Lorem.sentences(1) 
profile.status = "active"
position = user.build_position
position.lat = -73.994025+Faker::Number.number(4).to_i*0.00001 #=> "1968353479"
position.long = 40.752635+Faker::Number.number(4).to_i*0.00001
position.radio = Faker::Number.number(2) 
position.status = Faker::Number.number(2) 
user.interest_list.add(Faker::Lorem.words(4))
user.save!

zoom = Zoom.new
zoom.first_meeter_id = 1
zoom.second_meeter_id = 2
zoom.status = "matched"
zoom.save!

zoom = Zoom.new
zoom.first_meeter_id = 1
zoom.second_meeter_id = 4
zoom.status = "matched"
zoom.save!

zoom = Zoom.new
zoom.first_meeter_id = 1
zoom.second_meeter_id = 6
zoom.status = "matched"
zoom.save!

zoom = Zoom.new
zoom.first_meeter_id = 1
zoom.second_meeter_id = 16
zoom.status = "matched"
zoom.save!




60.times do
user = User.new
user.name = Faker::Name::first_name 
user.email = user.name+"@factory.com"
user.password = "password"
profile = user.build_profile
profile.company_name = Faker::Company.name
profile.image_url = rand(1..19).to_s+".jpg"
profile.company_position = Faker::Name.title
profile.description = Faker::Lorem.sentences(1) 
profile.status = "active"
position = user.build_position
position.lat = -73.994025+Faker::Number.number(4).to_i*0.00001 #=> "1968353479"
position.long = 40.752635+Faker::Number.number(4).to_i*0.00001
position.radio = Faker::Number.number(2) 
position.status = Faker::Number.number(2) 
user.interest_list.add(Faker::Lorem.words(4))
user.save!
end

