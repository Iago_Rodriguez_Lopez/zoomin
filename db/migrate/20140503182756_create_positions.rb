class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.float :lat
      t.float :long
      t.integer :radio
      t.integer :user_id
      t.string :status
      t.timestamps
    end
  end
end
