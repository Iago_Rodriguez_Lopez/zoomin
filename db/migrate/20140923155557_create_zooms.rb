class CreateZooms < ActiveRecord::Migration
  def change
    create_table :zooms do |t|
      t.integer :first_meeter_id
      t.integer :second_meeter_id
      t.string :status

      t.timestamps
    end
  end
end
