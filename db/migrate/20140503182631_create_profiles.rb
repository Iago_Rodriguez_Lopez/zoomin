class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :company_name
      t.string :company_position
      t.text :description
      t.integer :user_id
      t.string :status

      t.timestamps
    end
  end
end
